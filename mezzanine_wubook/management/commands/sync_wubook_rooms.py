from django.core.management.base import BaseCommand, CommandError
from mezzanine_wubook.utils import sync_wubook_rooms

class Command(BaseCommand):
    help = 'Sync wubook rooms'

    def handle(self, *args, **options):
        sync_wubook_rooms()