# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 04:48
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mezzanine_wubook', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WubookRoom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('wubook_id', models.CharField(max_length=50, verbose_name='Wubook ID')),
                ('name', models.CharField(blank=True, max_length=254, null=True, verbose_name='Name')),
                ('short_name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Short name')),
                ('occupancy', models.IntegerField(blank=True, null=True, verbose_name='Occupancy')),
                ('men', models.IntegerField(blank=True, null=True, verbose_name='Adults')),
                ('children', models.IntegerField(blank=True, null=True, verbose_name='Childrens')),
                ('subroom', models.CharField(blank=True, max_length=50, null=True, verbose_name='Parent Room')),
                ('anchorate', models.CharField(blank=True, max_length=100, null=True, verbose_name='Anchorate')),
                ('price', models.FloatField(blank=True, null=True, verbose_name='Price')),
                ('min_price', models.FloatField(blank=True, null=True, verbose_name='Min Price')),
                ('max_price', models.FloatField(blank=True, null=True, verbose_name='Max Price')),
                ('dec_avail', models.FloatField(blank=True, default=0, null=True, verbose_name='Dec available')),
                ('boards', models.CharField(blank=True, max_length=50, null=True, verbose_name='Boards')),
                ('board', models.CharField(blank=True, max_length=50, null=True, verbose_name='Board')),
                ('rtype', models.IntegerField(blank=True, null=True, verbose_name='R Type')),
                ('woodoo', models.IntegerField(blank=True, null=True, verbose_name='Woodo')),
                ('availability', models.IntegerField(blank=True, null=True, verbose_name='Availability')),
                ('rtype_name', models.CharField(blank=True, max_length=50, null=True, verbose_name='R Type Name')),
                ('last_sync', models.DateTimeField(auto_now=True)),
                ('deleted_in_wubook', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Wubook Room',
                'verbose_name_plural': 'Wubook Rooms',
            },
        ),
        migrations.CreateModel(
            name='WubookRoomImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('wubook_id', models.CharField(max_length=50, verbose_name='Wubook ID')),
                ('image_url', models.URLField(blank=True, null=True, verbose_name='Image Url')),
                ('last_sync', models.DateTimeField(auto_now=True)),
                ('deleted_in_wubook', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=True)),
                ('main_image', models.BooleanField(default=False)),
                ('wubook_room', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='mezzanine_wubook.WubookRoom', verbose_name='Wubook Room')),
            ],
        ),
    ]
