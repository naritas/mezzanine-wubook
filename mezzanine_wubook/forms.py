from datetime import timedelta

from django.utils import timezone
from django.utils.translation import ugettext as _
from django import forms
from mezzanine.conf import settings
from mezzanine.pages.admin import PageAdminForm

from mezzanine_wubook.models import WubookRoom


class WubookForm(forms.Form):
    dfrom = forms.DateField(
        label=_('Arrival date'),
        initial=timezone.now,
        input_formats=['%d/%m/%Y', ],
        widget=forms.DateInput(
            attrs={
                'class': 'form-control'
            }
        )
    )

    dto = forms.DateField(
        label=_('Departure date'),
        initial=timezone.now,
        input_formats=['%d/%m/%Y', ],
        widget=forms.DateInput(
            attrs={
                'class': 'form-control'
            }
        )
    )

    # nights = forms.ChoiceField(
    #     choices=(
    #         (1, 1),
    #     ),
    #     label=_('Nights'),
    #     widget=forms.Select(
    #         attrs={
    #             'class': 'form-control'
    #         }
    #     )
    # )

    # def __init__(self, *args, **kwargs):
    #
    #     super(WubookForm, self).__init__(*args, **kwargs)
    #     settings.use_editable()
    #
    #     try:
    #         max_nights = int(settings.WUBOOK_MAX_NIGHTS) + 1
    #
    #     except ValueError:
    #         max_nights = 30
    #
    #     choices_nights = [(i, i) for i in range(1, max_nights)]
    #     self.fields['nights'].choices = choices_nights

    def __init__(self, *args, **kwargs):
        super(WubookForm, self).__init__(*args, **kwargs)
        actual_date = timezone.now().date()
        self.fields['dfrom'].initial = actual_date.strftime('%d/%m/%Y')
        self.fields['dto'].initial = (actual_date + timedelta(days=1)).strftime('%d/%m/%Y')

    def get_action(self):
        settings.use_editable()
        return settings.WUBOOK_ACTION_FORM


class WubookRoomPageForm(PageAdminForm):

    class Meta:
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(WubookRoomPageForm, self).__init__(*args, **kwargs)

        self.fields['wubook_rooms'].queryset = WubookRoom.objects.filter(subroom=0)




