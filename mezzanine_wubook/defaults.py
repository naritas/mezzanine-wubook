from django.utils.translation import ugettext_lazy as _
from mezzanine.conf import register_setting
#
# register_setting(
#     name="WUBOOK_MAX_NIGHTS",
#     description=_("Max Nights in wubook form"),
#     editable=True,
#     default=30,
# )

register_setting(
    name="WUBOOK_ACTION_FORM",
    description=_("Url for action in wubook form"),
    editable=True,
    default='',
)

register_setting(
    name="WUBOOK_WIRED_API_URL",
    description=_("Wired API url"),
    label=_('Wired API url'),
    editable=True,
    default='https://wubook.net/xrws/',
)

register_setting(
    name="WUBOOK_WIRED_USER",
    label=_('Wired user'),
    editable=True,
    default='',
)

register_setting(
    name="WUBOOK_WIRED_PASSWORD",
    label=_('Wired password'),
    editable=True,
    default='',
)

register_setting(
    name="WUBOOK_WIRED_PROVIDER_KEY",
    label=_('Wired provider key'),
    editable=True,
    default='',
)

register_setting(
    name="WUBOOK_LCODE",
    label=_('Wubook LCODE'),
    editable=True,
    default='',
)


