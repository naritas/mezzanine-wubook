# coding=utf-8
from copy import deepcopy

from django.contrib import admin
from django.utils.translation import ugettext as _

from mezzanine_wubook.forms import WubookRoomPageForm
from mezzanine_wubook.utils import sync_wubook_rooms
from mezzanine_wubook.models import WubookPage, WubookRoom, WubookRoomImage, WubookRoomPage
from mezzanine.pages.admin import PageAdmin


wubookpage_extra_fields = (
    (
        _('Wubook Iframe Configuration'),
        {
            "fields": (
                ("content",),
                ("lcode", ),
                ("show_left_column", 'show_right_column', ),
                ("theme", 'lang', ),
                ("width", 'height', ),
                ('mobile', )
            )
        }
    ),
)


class WubookPageAdmin(PageAdmin):
    pass


class WubookRoomImageInline(admin.TabularInline):

    model = WubookRoomImage


class WubookRoomAdmin(admin.ModelAdmin):
    list_display = ('wubook_id', 'name', 'short_name', 'occupancy', 'subroom', 'active', 'deleted_in_wubook', 'last_sync')

    search_fields = ('wubook_id', 'subroom', 'name')

    list_filter = ('active', 'deleted_in_wubook', 'last_sync')

    inlines = (WubookRoomImageInline, )

    actions = ('resync_all_wubook_rooms', )

    def resync_all_wubook_rooms(self, request, queryset):
        synced_ids = sync_wubook_rooms()

        self.message_user(request, 'Synced {0} wubook rooms'.format(synced_ids))

    resync_all_wubook_rooms.short_description = "Resync all wubook rooms"


class WubookRoomPageAdmin(PageAdmin):

    filter_horizontal = ('wubook_rooms', )
    form = WubookRoomPageForm
    fieldsets = (
        (None, {
            "fields": [
                "title", "status",
                ("publish_date", "expiry_date"),
                'content',
                ('wubook_rooms', ),
                ("in_menus", ),
                ("login_required", )
            ],
        }),
        (_("Meta data"), {
            "fields": ["_meta_title", "slug",
                       ("description", "gen_description"),
                       "keywords", "in_sitemap"],
            "classes": ("collapse-closed",)
        }),
    )

admin.site.register(WubookPage, WubookPageAdmin)
admin.site.register(WubookRoomPage, WubookRoomPageAdmin)
admin.site.register(WubookRoom, WubookRoomAdmin)
