import xmlrpclib

from django.db.models.query_utils import Q
from django.utils import timezone
from mezzanine.conf import settings

from mezzanine_wubook.models import WubookRoom


def sync_wubook_rooms():
    server = xmlrpclib.Server(
        unicode(settings.WUBOOK_WIRED_API_URL.decode('utf-8'))
    )

    response_token, token = server.acquire_token(
        unicode(settings.WUBOOK_WIRED_USER.decode('utf-8')),
        unicode(settings.WUBOOK_WIRED_PASSWORD.decode('utf-8')),
        unicode(settings.WUBOOK_WIRED_PROVIDER_KEY.decode('utf-8'))
    )

    lcode = unicode(settings.WUBOOK_LCODE.decode('utf-8'))
    if response_token == 0:
        response_room, rooms = server.fetch_rooms(
            token,
            lcode
        )

        synced_ids = 0
        if response_room == 0:
            sync_ids = []
            for room in rooms:
                wubookroom, created = WubookRoom.objects.get_or_create(
                    wubook_id=room['id'],
                )
                sync_ids.append(room['id'])

                wubookroom.name = room.get('name')
                wubookroom.short_name = room.get('shortname')
                wubookroom.occupancy = room.get('occupancy')
                wubookroom.price = room.get('price')
                wubookroom.men = room.get('men')
                wubookroom.children = room.get('children')
                wubookroom.subroom = room.get('subroom')
                wubookroom.anchorate = room.get('anchorate')
                wubookroom.rtype = room.get('rtype')
                wubookroom.rtype_name = room.get('rtype_name')
                wubookroom.dec_avail = room.get('dec_avail')
                wubookroom.availability = room.get('availability')
                wubookroom.min_price = room.get('min_price')
                wubookroom.board = room.get('board')
                wubookroom.boards = room.get('boards')
                wubookroom.woodoo = room.get('woodoo')
                wubookroom.max_price = room.get('max_price')
                wubookroom.save()

                response_image, room_images = server.room_images(token, lcode, room['id'])

                if response_image == 0:
                    image_syncs = []
                    for image in room_images:
                        image_syncs.append(image['image_link'])
                        wubookroomimage, created = wubookroom.images.get_or_create(
                            # wubook_id=image['id_room']
                            image_url=image['image_link']
                        )

                        wubookroomimage.image_url = image['image_link']
                        wubookroomimage.main_image = True if image['main_image'] == 1 else False
                        wubookroomimage.save()

                    wubookroom.images.exclude(
                        Q(image_url__in=image_syncs) | Q(deleted_in_wubook=True)
                    ).update(
                        deleted_in_wubook=True,
                        active=False,
                        main_image=False,
                        last_sync=timezone.now()
                    )

            # disable no sync rooms
            WubookRoom.objects.exclude(
                Q(wubook_id__in=sync_ids) | Q(deleted_in_wubook=True)
            ).update(
                deleted_in_wubook=True,
                active=False,
                last_sync=timezone.now()
            )

            synced_ids = len(sync_ids)

        server.release_token(token)
        return synced_ids