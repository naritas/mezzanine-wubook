# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'WubookRoom'
        db.create_table(u'mezzanine_wubook_wubookroom', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('wubook_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
            ('short_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('occupancy', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('men', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('children', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('subroom', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('anchorate', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('min_price', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('max_price', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('dec_avail', self.gf('django.db.models.fields.FloatField')(default=0, null=True, blank=True)),
            ('boards', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('board', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('rtype', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('woodoo', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('availability', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('rtype_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('last_sync', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('deleted_in_wubook', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'mezzanine_wubook', ['WubookRoom'])

        # Adding model 'WubookRoomImage'
        db.create_table(u'mezzanine_wubook_wubookroomimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('wubook_room', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['mezzanine_wubook.WubookRoom'])),
            ('wubook_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('image_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('last_sync', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('deleted_in_wubook', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('main_image', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'mezzanine_wubook', ['WubookRoomImage'])


    def backwards(self, orm):
        # Deleting model 'WubookRoom'
        db.delete_table(u'mezzanine_wubook_wubookroom')

        # Deleting model 'WubookRoomImage'
        db.delete_table(u'mezzanine_wubook_wubookroomimage')


    models = {
        u'mezzanine_wubook.wubookpage': {
            'Meta': {'ordering': "(u'_order',)", 'object_name': 'WubookPage', '_ormbases': [u'pages.Page']},
            'class_content': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'content': ('mezzanine.core.fields.RichTextField', [], {}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'}),
            'lcode': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'mobile': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            u'page_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['pages.Page']", 'unique': 'True', 'primary_key': 'True'}),
            'show_left_column': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_right_column': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'theme': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'mezzanine_wubook.wubookroom': {
            'Meta': {'object_name': 'WubookRoom'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'anchorate': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'availability': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'board': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'boards': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'children': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'dec_avail': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'deleted_in_wubook': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_sync': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'max_price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'men': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'min_price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'occupancy': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'rtype': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rtype_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'subroom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'woodoo': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'wubook_id': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'mezzanine_wubook.wubookroomimage': {
            'Meta': {'object_name': 'WubookRoomImage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'deleted_in_wubook': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'last_sync': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'main_image': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'wubook_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'wubook_room': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['mezzanine_wubook.WubookRoom']"})
        },
        u'pages.page': {
            'Meta': {'ordering': "(u'titles',)", 'object_name': 'Page'},
            '_meta_title': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            '_order': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'content_model': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'gen_description': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_menus': ('mezzanine.pages.fields.MenusField', [], {'default': '(1, 2, 3)', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'keywords_string': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'login_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'children'", 'null': 'True', 'to': u"orm['pages.Page']"}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'short_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'titles': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'null': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "(u'domain',)", 'object_name': 'Site', 'db_table': "u'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['mezzanine_wubook']