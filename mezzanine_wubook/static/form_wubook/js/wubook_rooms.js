$(function () {
    var lcode = window.lcode;
    var rooms = window.rooms;

    window.template_widget_room = _.template($('#widget_room_template').html());

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var $form_wubook = $('.form-wubook-room');

    WuBook = new _WuBook(lcode);

    $form_wubook.find('input[name=dfrom]').datepicker({
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        },
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (ev) {

    }).data('datepicker');

    $form_wubook.find('input[name=dto]').datepicker({
        format: 'dd/mm/yyyy',
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        },
    }).on('changeDate', function (ev) {
    }).data('datepicker');

    $form_wubook.find('.btn-search-rooms').on('click', function () {
        var $room = $(this).closest('.wubook-room');
        var $dfrom = $room.find('.form-wubook-room').find('input[name=dfrom]');
        var $dto = $room.find('.form-wubook-room').find('input[name=dto]');
        checkRooms($dfrom.val(), $dto.val(), $room.data('room-id'));
    });

    function get_prices(data, room_id) {
        room_id = parseInt(room_id);
        var results = {};
        _.mapObject(data['results'][lcode], function (val, key) {
            results = _.extendOwn(results, val);
        });

        window.results = results;
        var prices = {};

        _.keys(window.rooms[room_id]).forEach(function (rid) {
            prices[rid] = results[rid];
        });

        return prices;
    }

    function generate_widgets(prices, room_id, dfrom, dto){
        var $wubook_room = $('.wubook-room[data-room-id='+ room_id +']').find('.room-results');
        var exists_price = false;
        _.mapObject(prices, function (price, rid) {
            if (price){
                var room = window.rooms[room_id][rid];

                var params = { dfrom: dfrom, dto: dto, bookrooms: rid };
                var action_url = window.booking_url + '?' + $.param(params);
                $wubook_room.append(
                    window.template_widget_room(
                        {
                            'adults': room['adults'],
                            'childrens': room['children'],
                            'price': price,
                            'action': action_url,
                        }
                    )
                );
                exists_price = true;
            }
        });
        if (!exists_price){
            $wubook_room.append('<div class="alert alert-warning">No hay habitaciones disponibles</div>'); // TODO: Add translations
        }
    }

    function checkRooms(dfrom, dto, room_id) {
        var $wubook_room = $('.wubook-room[data-room-id='+ room_id +']').find('.room-results');
        $wubook_room.html('');

        WuBook.fount({'dfrom': dfrom, 'dto': dto, 'compact': 0}, function (data) {
            generate_widgets(get_prices(data, room_id), room_id, dfrom, dto);
        })
    }

});