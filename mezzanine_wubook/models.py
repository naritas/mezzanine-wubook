# coding=utf-8
import json

from django.db import models
from django.utils.translation import ugettext_lazy as _

from mezzanine.pages.models import Page
from mezzanine.core.models import RichText
from mezzanine.conf import settings


WOOBOOK_THEMES = (
    ('default', 'Default'),
    ('ammende', 'ammende'),
    ('asphalt', 'asphalt'),
    ('atlantic', 'atlantic'),
    ('autosole', 'autosole'),
    ('aventinn', 'aventinn'),
    ('avrora', 'avrora'),
    ('bear', 'bear'),
    ('compact', 'compact'),
    ('demetra', 'demetra'),
    ('dianapalace', 'dianapalace'),
    ('dostoevsky', 'dostoevsky'),
    ('forest', 'forest'),
    ('hm', 'hm'),
    ('hotelpeople', 'hotelpeople'),
    ('hotelsteam', 'hotelsteam'),
    ('marshal', 'marshal'),
    ('nautilusinn', 'nautilusinn'),
    ('octaviana', 'octaviana'),
    ('orangewhite', 'orangewhite'),
    ('paraiso', 'paraiso'),
    ('purple', 'purple'),
    ('pushkin', 'pushkin'),
    ('residencemoika', 'residencemoika'),
    ('serious', 'serious'),
    ('shelfort', 'shelfort'),
    ('sofi', 'sofi'),
    ('symfony', 'symfony'),
    ('triangle', 'triangle'),
    ('vergaz', 'vergaz'),
    ('wcdonald', 'wcdonald'),
    ('wubook', 'wubook'),
    ('wugle', 'wugle'),
    ('zak', 'zak'),
    ('zizi', 'zizi'),
)


WUBOOK_LANG = (
    ('ca', 'ca'),
    ('cs', 'cs'),
    ('de', 'de'),
    ('ee', 'ee'),
    ('en', 'en'),
    ('es', 'es'),
    ('fi', 'fi'),
    ('fr', 'fr'),
    ('gr', 'gr'),
    ('hr', 'hr'),
    ('it', 'it'),
    ('ko', 'ko'),
    ('lv', 'lv'),
    ('nl', 'nl'),
    ('pt', 'pt'),
    ('ro', 'ro'),
    ('ru', 'ru'),
    ('uk', 'uk'),
)


WUBOOK_MOBILE = (
    (0, _('No')),
    (1, _('Yes')),
)


class WubookPage(Page, RichText):

    class_content = models.CharField(
        verbose_name=_('Class content'),
        max_length=50,
    )

    lcode = models.CharField(
        verbose_name=_('Property code'),
        max_length=50,
    )

    show_left_column = models.BooleanField(
        verbose_name=_('Show left column'),
        default=False
    )

    show_right_column = models.BooleanField(
        verbose_name=_('Show right column'),
        default=False
    )

    theme = models.CharField(
        verbose_name=_('Booking theme'),
        max_length=50,
        choices=WOOBOOK_THEMES,
    )

    mobile = models.SmallIntegerField(
        verbose_name=_('Open in new window in mobile devices'),
        choices=WUBOOK_MOBILE,
        default=0,
    )

    lang = models.CharField(
        verbose_name=_('Language'),
        max_length=5,
        choices=WUBOOK_LANG,
        blank=True
    )

    width = models.PositiveIntegerField(
        verbose_name=_('Width'),
        help_text=_('Value in pixels (px). To stop automatic leave the field value 0'),
        default=0
    )

    height = models.PositiveIntegerField(
        verbose_name=_('Height'),
        help_text=_('Value in pixels (px). To stop automatic leave the field value 0'),
        default=0
    )

    class Meta:
        verbose_name = _('Wubook Page')
        verbose_name_plural = _('Wubook Pages')


class WubookRoomPage(Page, RichText):

    wubook_rooms = models.ManyToManyField('WubookRoom', verbose_name=_('Wubook rooms'))

    def get_lcode(self):
        return unicode(settings.WUBOOK_LCODE.decode('utf-8'))

    def get_booking_url(self):
        return unicode(settings.WUBOOK_ACTION_FORM.decode('utf-8'))

    def get_rooms_json(self):
        wubookrooms = {}
        for wubookroom in self.wubook_rooms.all():
            rooms_childs = {}
            for wubookroomchild in WubookRoom.objects.filter(subroom=wubookroom.wubook_id):
                rooms_childs[wubookroomchild.wubook_id] = {
                    'adults': wubookroomchild.men,
                    'children': wubookroomchild.children,
                }

            rooms_childs[wubookroom.wubook_id] = {
                'adults': wubookroom.men,
                'children': wubookroom.children,
            }
            wubookrooms[wubookroom.wubook_id] = rooms_childs

        return json.dumps(wubookrooms)

    class Meta:
        verbose_name = _('Wubook Room Page')
        verbose_name_plural = _('Wubook Room Pages')


class WubookRoom(models.Model):

    wubook_id = models.CharField(_('Wubook ID'), max_length=50)
    name = models.CharField(_('Name'), max_length=254, blank=True, null=True)
    short_name = models.CharField(_('Short name'), max_length=50, blank=True, null=True)
    occupancy = models.IntegerField(_('Occupancy'), blank=True, null=True)
    men = models.IntegerField(_('Adults'), blank=True, null=True)
    children = models.IntegerField(_('Childrens'), blank=True, null=True)
    subroom = models.CharField(verbose_name=_('Parent Room'), max_length=50, blank=True, null=True)
    anchorate = models.CharField(verbose_name=_('Anchorate'), blank=True, null=True, max_length=100)
    price = models.FloatField(_('Price'), blank=True, null=True)
    min_price = models.FloatField(_('Min Price'), blank=True, null=True)
    max_price = models.FloatField(_('Max Price'), blank=True, null=True)
    dec_avail = models.FloatField(_('Dec available'), blank=True, null=True, default=0)
    boards = models.CharField(_('Boards'), max_length=50, blank=True, null=True)
    board = models.CharField(_('Board'), max_length=50, blank=True, null=True)
    rtype = models.IntegerField(_('R Type'), blank=True, null=True)
    woodoo = models.IntegerField(_('Woodo'), blank=True, null=True)
    availability = models.IntegerField(_('Availability'), blank=True, null=True)
    rtype_name = models.CharField(_('R Type Name'), max_length=50, blank=True, null=True)
    description = models.TextField(_('Description'), blank=True, null=True)

    last_sync = models.DateTimeField(auto_now=True)
    deleted_in_wubook = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('Wubook Room')
        verbose_name_plural = _('Wubook Rooms')

    def __unicode__(self):
        return self.name

    def get_main_image(self):
        return self.images.filter(active=True, main_image=True).first()

    def get_images_without_main(self):
        return self.images.filter(active=True, main_image=False)


class WubookRoomImage(models.Model):

    wubook_room = models.ForeignKey('WubookRoom', verbose_name=_('Wubook Room'), related_name='images')
    image_url = models.URLField(verbose_name=_('Image Url'), blank=True, null=True)

    last_sync = models.DateTimeField(auto_now=True)
    deleted_in_wubook = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    main_image = models.BooleanField(default=False)


